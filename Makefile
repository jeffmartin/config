# ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~
# System Configuration Install:
#
# This makefile is used to install the config files.
# Files are installed by creating symlinks. 
#
# **NOTE: Running 'make' will overwrite any config files that already exist.**
#
# Jeffrey Martin - jeffrey.martin04@gmail.com
# ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~

REPO_DIR = $(shell pwd)

all:
	make configure-i3
	make configure-lightdm
	make configure-mate
	make configure-picom
	make configure-polybar
	make configure-regolith
	make configure-vim
	make configure-vs-code
	make configure-wallpaper
	make configure-x
	make configure-zsh

# ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~
# Install Individual Configuration files
# ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~

configure-i3:
	mkdir -p $(HOME)/.config/i3
	ln -sf $(REPO_DIR)/i3/i3wm.conf $(HOME)/.config/i3/config
	ln -sf $(REPO_DIR)/i3/goto_new_workspace.sh $(HOME)/.config/i3/goto_new_workspace.sh
	ln -sf $(REPO_DIR)/i3/moveto_new_workspace.sh $(HOME)/.config/i3/moveto_new_workspace.sh
	ln -sf $(REPO_DIR)/i3/reset_displays.sh $(HOME)/.config/i3/reset_displays.sh

configure-lightdm:
	sudo cp $(REPO_DIR)/lightdm/lightdm.conf /etc/lightdm/lightdm-gtk-greeter.conf

configure-mate:
	dconf load /org/mate/ < $(REPO_DIR)/mate/mate.dconf
	sudo cp $(REPO_DIR)/mate/reset_panel.sh /usr/local/bin/reset_panel.sh
	sudo chmod 777 /usr/local/bin/reset_panel.sh

save-mate:
	dconf dump /org/mate/ > $(REPO_DIR)/mate/mate.dconf

configure-picom:
	ln -sf $(REPO_DIR)/picom/picom.conf $(HOME)/.config/picom.conf

configure-polybar:
	mkdir -p $(HOME)/.config/polybar
	ln -sf $(REPO_DIR)/polybar/bar.conf $(HOME)/.config/polybar/config.ini

configure-regolith:
	mkdir -p $(HOME)/.config/regolith3/i3
	ln -sf $(REPO_DIR)/i3/i3-for-regolith.conf $(HOME)/.config/regolith3/i3/config

configure-vim:
	ln -sf $(REPO_DIR)/vim/vimrc $(HOME)/.vimrc

configure-vs-code:
	mkdir -p $(HOME)/.config/Code/User
	ln -sf $(REPO_DIR)/vs-code/user-settings.json $(HOME)/.config/Code/User/settings.json

configure-wallpaper:
	sudo cp $(REPO_DIR)/wallpaper/wallpaper.jpg /usr/share/backgrounds/wallpaper.jpg
	sudo chmod 777 /usr/share/backgrounds/wallpaper.jpg
	sudo cp $(REPO_DIR)/wallpaper/wallpaper.jpg /etc/lightdm/wallpaper.jpg
	sudo cp $(REPO_DIR)/wallpaper/reset_wallpaper.sh /usr/local/bin/reset_wallpaper.sh
	sudo chmod 777 /usr/local/bin/reset_wallpaper.sh

configure-x:
	ln -sf $(REPO_DIR)/X11/xmodmap.conf $(HOME)/.xmodmap
	ln -sf $(REPO_DIR)/X11/xinitrc $(HOME)/.xinitrc
	sudo cp $(REPO_DIR)/X11/source_xinitrc.sh /usr/local/bin/source_xinitrc.sh
	sudo chmod 777 /usr/local/bin/source_xinitrc.sh

configure-zsh:
	ln -sf $(REPO_DIR)/zsh/zshrc $(HOME)/.zshrc

