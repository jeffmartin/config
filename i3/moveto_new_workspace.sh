#!/bin/bash
WS_JSON=$(i3-msg -t get_workspaces)
i=1;
while [ 1 ]
do
    if [[ $WS_JSON != *"\"num\":$i"* ]]; then
        i3-msg move container to workspace number $i
        i3-msg workspace number $i
        break;
    fi
    i=$((i+1))
done
