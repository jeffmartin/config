#!/bin/bash

# Script to move workspaces to the appropriate display.
# Workspace 1 is left on the 'original' display, and all other workspaces
# are moved to the primary display.
WS_JSON=$(i3-msg -t get_workspaces)
i=2;  # Start at 2 to not update workspace 1
while [ 1 ]
do
    if [[ $WS_JSON == *"\"num\":$i"* ]]; then
        i3-msg workspace number $i
        i3-msg move workspace to output primary
    else
      if [[ $i > 1 ]]; then
          i3-msg workspace number 2
      else
        i3-msg workspace number 1
      fi
      break
    fi
    i=$((i+1))
done
