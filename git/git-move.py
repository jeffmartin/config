import argparse
import subprocess

def run_git_command(cmd):
    # Pipe the command into cat to drop Git's pager.
    return subprocess.getoutput(cmd + ' | cat')

def is_valid_branch(branch):
    branches = run_git_command('git branch')
    branches = branches.split('\n')
    branches = [branch.replace('*', '').strip() for branch in branches]
    return branch in branches

def parent(branch):
    return run_git_command('git log --pretty=%p -n 1 ' + branch)

def move(src, dst):
    output = run_git_command('git rebase --onto ' + dst + ' ' + parent(src) + ' ' + src)
    print(output)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--src", help="The branch to be moved")
    parser.add_argument("--dst", help="The target of the move")
    args = parser.parse_args()
    if not args.src:
        print('missing required "--src" argument')
        exit(-1)
    if not args.dst:
        print('missing required "--dst" argument')
        exit(-1)

    if not is_valid_branch(args.src):
        print('source branch "' + args.src + '" is not a valid branch')
        exit(-1)
    if not is_valid_branch(args.dst):
        print('destination branch "' + args.dst + '" is not a valid branch')
        exit(-1)



    print('moving branch "' + args.src + '" onto branch "' + args.dst + '".')

    move(args.src, args.dst)

if __name__ == "__main__":
    main()
