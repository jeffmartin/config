# System Configuration Files

## Required Applications
The following applications can be installed via `sudo apt install ...`
* mate-desktop-environment
* i3
* picom
* zsh
* fonts-powerline
* vim

The following applications require custom installation
* [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh#basic-installation)
* [Google Chrome](https://www.google.com/chrome/?platform=linux)
* [System76 Drivers](https://support.system76.com/articles/install-ubuntu/)
* [mate-i3-applet](https://github.com/city41/mate-i3-applet)
  * Note that you'll probably need to install [python-distutils](https://stackoverflow.com/questions/55749206/modulenotfounderror-no-module-named-distutils-core)

